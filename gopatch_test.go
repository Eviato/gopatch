package main

import (
	"testing"
)

// TestAlphaUseCase
func TestAlphaUseCase(t *testing.T) {
	method := "alpha"
	tests := [][]string{
		{"0.0.0-alpha", "0.0.0-alpha.1"},
		{"0.0.0-alpha.1", "0.0.0-alpha.2"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}

// TestBetaUseCase
func TestBetaUseCase(t *testing.T) {
	method := "beta"
	tests := [][]string{
		{"0.0.0-alpha", "0.0.0-beta"},
		{"0.0.0-alpha.1", "0.0.0-beta"},
		{"0.0.0-beta", "0.0.0-beta.1"},
		{"0.0.0-beta.1", "0.0.0-beta.2"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}

// TestRCUseCase
func TestRCUseCase(t *testing.T) {
	method := "rc"
	tests := [][]string{
		{"0.0.0-alpha", "0.0.0-rc"},
		{"0.0.0-alpha.1", "0.0.0-rc"},
		{"0.0.0-beta", "0.0.0-rc"},
		{"0.0.0-beta.1", "0.0.0-rc"},
		{"0.0.0-rc", "0.0.0-rc.1"},
		{"0.0.0-rc.1", "0.0.0-rc.2"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}

// TestReleaseUseCase
func TestReleaseUseCase(t *testing.T) {
	method := "release"
	tests := [][]string{
		{"0.0.0-alpha", "0.0.0"},
		{"0.0.0-alpha.1", "0.0.0"},
		{"0.0.0-beta", "0.0.0"},
		{"0.0.0-beta.1", "0.0.0"},
		{"0.0.0-rc", "0.0.0"},
		{"0.0.0-rc.1", "0.0.0"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}

// TestPatchUseCase
func TestPatchUseCase(t *testing.T) {
	method := "patch"
	tests := [][]string{
		{"0.0.0-alpha", "0.0.1"},
		{"0.0.0-alpha.1", "0.0.1"},
		{"0.0.0-beta", "0.0.1"},
		{"0.0.0-beta.1", "0.0.1"},
		{"0.0.0-rc", "0.0.1"},
		{"0.0.0-rc.1", "0.0.1"},
		{"0.0.1", "0.0.2"},
		{"0.1.0", "0.1.1"},
		{"1.0.0", "1.0.1"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}

// TestMinorUseCase
func TestMinorUseCase(t *testing.T) {
	method := "minor"
	tests := [][]string{
		{"0.0.0-alpha", "0.1.0"},
		{"0.0.0-alpha.1", "0.1.0"},
		{"0.0.0-beta", "0.1.0"},
		{"0.0.0-beta.1", "0.1.0"},
		{"0.0.0-rc", "0.1.0"},
		{"0.0.0-rc.1", "0.1.0"},
		{"0.0.1", "0.1.0"},
		{"0.1.0", "0.2.0"},
		{"1.0.0", "1.1.0"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}

// TestMajorUseCase
func TestMajorUseCase(t *testing.T) {
	method := "major"
	tests := [][]string{
		{"0.0.0-alpha", "1.0.0"},
		{"0.0.0-alpha.1", "1.0.0"},
		{"0.0.0-beta", "1.0.0"},
		{"0.0.0-beta.1", "1.0.0"},
		{"0.0.0-rc", "1.0.0"},
		{"0.0.0-rc.1", "1.0.0"},
		{"0.0.1", "1.0.0"},
		{"0.1.0", "1.0.0"},
		{"1.0.0", "2.0.0"},
	}

	for _, test := range tests {
		newVersion := rootUpgrade(test[0], method)
		if newVersion != test[1] {
			t.Errorf("Updated version with %s was incorrect, got: %s, want: %s.", method, newVersion, test[1])
		}
	}
}
