# PHASE 1 ; build the binarie
FROM golang:alpine as builder

# install some builder dependencies
RUN apk update && apk add git ca-certificates curl

# install dep
RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 && chmod +x /usr/local/bin/dep

# manage path
COPY . $GOPATH/src/Eviato/gopatch/
WORKDIR $GOPATH/src/Eviato/gopatch/

# install dependencies app
RUN dep ensure -vendor-only -v

# build the app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/gopatch

# PHASE 2 ; build the image
FROM alpine

# install necessary package
RUN apk update && apk add git openssh-client

# copy necessary file
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /go/bin/gopatch /go/bin/gopatch

CMD ["sh"]
