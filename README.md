# GoPatch

GoPatch help you to manage you application version by returning the updated version

## Documentation

```
Usage: gopatch [global options] <verb> [verb options]

Global options:
        -h, --help        Show this help

Verbs:
    major:
        -v, --version     version to patch (*)
    minor:
        -v, --version     version to patch (*)
    patch:
        -v, --version     version to patch (*)
    alpha:
        -v, --version     version to patch (*)
    beta:
        -v, --version     version to patch (*)
    rc:
        -v, --version     version to patch (*)
```

## Unit Test

In project folder
```
$ go test -v
```
