package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/voxelbrain/goptions"
)

type command struct {
	Help goptions.Help `goptions:"-h, --help, description='Show this help'"`
	goptions.Verbs
	Major struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"major"`
	Minor struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"minor"`
	Patch struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"patch"`
	Alpha struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"alpha"`
	Beta struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"beta"`
	Rc struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"rc"`
	Release struct {
		Version string `goptions:"-v, --version, obligatory, description='version to patch'"`
	} `goptions:"release"`
}

type version struct {
	major             int
	minor             int
	patch             int
	preRelease        string
	preReleaseVersion int
}

func main() {
	options := command{}

	// Parse flags and verbs, handle error or missing mandatory parameter
	goptions.ParseAndFail(&options)

	// no subcommand just display help
	if options.Verbs == "" {
		goptions.PrintHelp()
	}
	var versionUpdated string
	switch options.Verbs {
	case "major":
		versionUpdated = rootUpgrade(options.Major.Version, "major")
	case "minor":
		versionUpdated = rootUpgrade(options.Minor.Version, "minor")
	case "patch":
		versionUpdated = rootUpgrade(options.Patch.Version, "patch")
	case "alpha":
		versionUpdated = rootUpgrade(options.Alpha.Version, "alpha")
	case "beta":
		versionUpdated = rootUpgrade(options.Beta.Version, "beta")
	case "rc":
		versionUpdated = rootUpgrade(options.Rc.Version, "rc")
	default:
		goptions.PrintHelp()
	}
	fmt.Println(versionUpdated)
}

func rootUpgrade(value string, method string) string {
	vObj := version{}
	uObj := version{}
	if validVersion(value) {
		vObj = parseVersion(value)
	}
	uObj = updateVersion(vObj, method)
	return renderVersion(uObj)
}

func validVersion(v string) bool {
	v = strings.Trim(v, " ")

	re, _ := regexp.Compile(`^(\d*[.]){2}\d*((-(beta|alpha|rc)([.]\d+)?)?)$`)
	if re.MatchString(v) {
		return true
	}
	return false
}

func parseVersion(v string) version {
	ret := version{}

	// Extract Major Minor Patch
	twice := strings.Split(v, `-`)

	// major, minor, patch
	mmp := strings.Split(twice[0], `.`)
	ret.major, _ = strconv.Atoi(mmp[0])
	ret.minor, _ = strconv.Atoi(mmp[1])
	ret.patch, _ = strconv.Atoi(mmp[2])

	// pre-release, pre-release-version
	if len(twice) > 1 {
		prprv := strings.Split(twice[1], `.`)
		ret.preRelease = prprv[0]
		if len(prprv) > 1 {
			ret.preReleaseVersion, _ = strconv.Atoi(prprv[1])
		}
	}
	return ret
}

func updateVersion(v version, verbs string) version {
	switch verbs {
	case "major":
		v.major = v.major + 1
		v.minor = 0
		v.patch = 0
		v.preRelease = ""
		v.preReleaseVersion = 0
	case "minor":
		v.minor = v.minor + 1
		v.patch = 0
		v.preRelease = ""
		v.preReleaseVersion = 0
	case "patch":
		v.patch = v.patch + 1
		v.preRelease = ""
		v.preReleaseVersion = 0
	case "alpha":
		if v.preRelease == "" {
			v.patch = v.patch + 1
		}
		if v.preRelease != "alpha" {
			v.preReleaseVersion = 0
		} else {
			v.preReleaseVersion = v.preReleaseVersion + 1
		}
		v.preRelease = "alpha"
	case "beta":
		if v.preRelease != "beta" {
			v.preReleaseVersion = 0
		} else {
			v.preReleaseVersion = v.preReleaseVersion + 1
		}
		v.preRelease = "beta"
	case "rc":
		if v.preRelease != "rc" {
			v.preReleaseVersion = 0
		} else {
			v.preReleaseVersion = v.preReleaseVersion + 1
		}
		v.preRelease = "rc"
	case "release":
		v.preRelease = ""
		v.preReleaseVersion = 0
	}
	return v
}

func renderVersion(v version) string {
	var a []string
	a = append(a, strconv.Itoa(v.major), strconv.Itoa(v.minor), strconv.Itoa(v.patch))
	sV := strings.Join(a, ".")

	if v.preRelease != "" {
		sV = sV + "-" + v.preRelease
		if v.preReleaseVersion != 0 {
			sV = sV + "." + strconv.Itoa(v.preReleaseVersion)
		}
	}

	return sV
}
